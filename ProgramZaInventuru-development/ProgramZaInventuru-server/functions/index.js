const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

var admin = require("firebase-admin");

var serviceAccount = require("./programzainventuru-firebase-adminsdk-rqpov-29a63f55d3.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});
const db = admin.firestore();


app.get('/getstavka', (request, response) => {
  db.collection('stavkaInv')
    .orderBy('naziv', 'asc')//mijenjamo za filtre
    .get()
    .then(querySnapshot => {
      let docPromises = [];
      querySnapshot.forEach(doc => {
        let document = {
          id: doc.id,
          data: doc.data(),
          subCollections: {}
        };

        let subCollectionPromise = doc.ref.listCollections().then(collections => {
          let collectionPromises = collections.map(collection => {
            return collection.get().then(subSnapshot => {
              let subDocuments = [];
              subSnapshot.forEach(subDoc => {
                subDocuments.push({
                  id: subDoc.id,
                  data: subDoc.data()
                });
              });
              document.subCollections[collection.id] = subDocuments;
            });
          });
          return Promise.all(collectionPromises).then(() => document);
        });

        docPromises.push(subCollectionPromise);
      });

      return Promise.all(docPromises);
    })
    .then(results => {
      response.send(results);
    })
    .catch(error => {
      console.error("Error getting documents or subcollections: ", error);
      response.status(500).send("Error getting documents: " + error.message);
    });
});

app.get('/getzaposlenik', (request, response) => {
  let res = [];
  db.collection('zaposlenik')
    .orderBy('naziv', 'asc')//mijenjamo za filtre
    .get()
    .then((querySnapshot) => {
      querySnapshot.forEach((doc) => {
        let document = {
          id: doc.id,
          data: doc.data()
        }
        res.push(document)
      })
      return response.send(res)
    })
    .catch((error) => {
      return response.send("Error getting documents: ", error);
    })
})



app.post('/addstavka', (request, response) => {
  if (Object.keys(request.body).length) {
    db.collection('stavkaInv').doc().set(request.body)
      .then(function () {
        return response.send(
          "Document successfully written - created!"
        )
      })
      .catch(function (error) {
        return response.send(
          "Error writing document: " + error
        )
      })
  } else {
    return response.send(
      "No post data for new document. " +
      "A new document is not created!"
    )
  }
})



app.put('/updatestavka', (request, response) => {
  if (Object.keys(request.body).length) {
    if (typeof request.query.id !== 'undefined') {
      db.collection('stavkaInv')
        .doc(request.query.id)
        .update(request.body)
        .then(function () {
          return response.send(
            "Document successfully written - " +
            "updated!"
          )
        })
        .catch(function (error) {
          return response.send(
            "Error writing document: " + error
          )
        })
    } else {
      return response.send(
        "A parameter id is not set. " +
        "A document is not updated!"
      )
    }
  } else {
    return response.send(
      "No post data for new document. " +
      "A document is not updated!"
    )
  }
})

app.delete('/deletestavka', (request, response) => {
  if (typeof request.query.id !== 'undefined') {
    db.collection('stavkaInv').doc(request.query.id).delete()
      .then(function () {
        return response.send(
          "Document successfully deleted!"
        )
      })
      .catch(function (error) {
        return response.send(
          "Error removing document: " + error
        )
      })
  } else {
    return response.send(
      "A parameter id is not set. " +
      "A document is not deleted!"
    )
  }
})


app.listen(3000, () => {
  console.log("Server running on port 3000");
});
