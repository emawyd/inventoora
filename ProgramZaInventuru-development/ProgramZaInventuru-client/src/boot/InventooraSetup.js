import { createApp } from "vue";
import { initializeApp } from 'firebase/app'
import { getAuth } from 'firebase/auth'
import { getFirestore } from 'firebase/firestore'
import { getStorage } from 'firebase/storage'

const app = createApp({})
const firebaseConfig = {
    apiKey: 'AIzaSyAwjiwCU4AjrXzh1V06BBE6tmrWyODPCl0',
    authDomain: 'programzainventuru.firebaseapp.com',
    projectId: 'programzainventuru',
    storageBucket: 'programzainventuru.appspot.com',
    messagingSenderId: '844750633592',
    appId: '1:844750633592:web:3921f6dfda570cd6104b8d',
    measurementId: 'G-7F99S9SXEZ'
}
const firebaseApp = initializeApp(firebaseConfig)
app.config.globalProperties.$auth = getAuth(firebaseApp)
app.config.globalProperties.$db = getFirestore(firebaseApp)
app.config.globalProperties.$storage = getStorage(firebaseApp)
export default app