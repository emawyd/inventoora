const routes = [
  {
    path: '/Login',
    component: () => import('layouts/LoginPageLayout.vue'),
    children: [
      { path: '/', component: () => import('pages/Login/LoginIndex.vue') }
    ]
    },

    {
    path: '/Administration',
    component: () => import('layouts/InventooraLayout.vue'),
    meta: { auth: true },
    children: [
      {
        path: '/Stavka', meta: { auth: true }, component: () => import('pages/Inventoora/StavkaIndex.vue')
      },
      {
        path: '/Zaposlenik', meta: { auth: true }, component: () => import('pages/Inventoora/ZaposlenikIndex.vue')
      }
    ]


  }
]

// Always leave this as last one,
// but you can also remove it
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  })
}



export default routes
