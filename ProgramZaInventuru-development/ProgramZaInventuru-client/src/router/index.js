import app from '../boot/InventooraSetup.js'
import { createRouter, createWebHashHistory } from 'vue-router'
import routes from './routes'

const isUserLoggedIn = () => {
  const $auth = app.config.globalProperties.$auth
  return new Promise((resolve, reject) => {
    const unsubscribeOnAuthStateChanged = $auth.onAuthStateChanged(theUser => {
      resolve(theUser)
      unsubscribeOnAuthStateChanged()
    }, err => {
      console.error(err)
      resolve(null)
      unsubscribeOnAuthStateChanged()
    })
  })
}
export default function () {
  const Router = createRouter({
    history: createWebHashHistory(),
    routes
  })
  app.use(Router)
  Router.beforeEach(async (to, from, next) => {
    if (to.matched.some(record => record.meta.auth)) {
      await isUserLoggedIn()
        .then(res => {
          if (res) {
            next()
          } else {
            next('/')
          }
        })
    } else {
      next()
    }
  })
  return Router
}
